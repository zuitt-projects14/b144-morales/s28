/*console.log("Hello World")*/

fetch("https://jsonplaceholder.typicode.com/todos").then(response => response.json())
.then(data => {
	console.log(data)
})

//Number4
/*fetch("https://jsonplaceholder.typicode.com/todos").then(response => response.json())
.then(data => {
	let dataMap = data.map(
	function(title){
		return `${title}`
		}
	)
	console.log(`${data.title}`)
})*/

fetch("https://jsonplaceholder.typicode.com/todos").then(response => response.json())
.then(data => {
	let titles = data.map((title) => title.title);

		console.log(titles);
})





/*
let mapDays = days.map(
	function(day){
		return `${day} is the day og the week`
		}
	)

console.log(mapDays);
console.log(days);
*/


//Number 5
fetch("https://jsonplaceholder.typicode.com/todos/1").then(response => response.json())//parse the response as JSON
.then(data => {
	console.log(data)//process the result
})

//Number 6

fetch("https://jsonplaceholder.typicode.com/todos/1").then(response => response.json())
.then(data => {
	console.log(`The item ${data.title} on the list has a status of ${data.completed}`)
})


//Number 7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	//Sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		userId: 100,
		completed: false,
		title: "New Todos"
	})
})
.then(res => res.json())
.then(data => console.log(data))



//Number 8

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do with a different data structure",
        id: 1,
        title: "updated to do list item",
        userId: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))


//Number 9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: "delectus aut autem",
		description: "Updated",
 		status: "Complete",
		dateCompleted: "07/09/21",
		userId: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))


//Number 10


fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: "patch updated delectus aut autem",
		description: "Patch Updated",
 		status: "Complete",
		dateCompleted: "07/09/21",
		userId: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))


//number 11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        status: false
    })
})
.then(response => response.json())
.then(data => console.log(data))


//Number 12
fetch('https://jsonplaceholder.typicode.com/todos/3', {
    method: 'DELETE',
    
    
})
.then(response => response.json())
.then(data => console.log(data))


































