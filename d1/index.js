/*console.log("Hello World");*/
//Javascript Synchronous Vs Asynchronous

//Synchronous Javascript
//In synchronous operations tasks are performed one at a time and only when one is completed, the following is unblocked
//Javascript is by default is synchronous
console.log("Hello");
console.log("Hello Again");


/*for(let i = 0; i <= 15; i++){
	console.log(i)
}*/
//blocking is when the execution of additional js process must wait until the operation complete.
console.log("Goodbye");

//Asynchronous Javascript
//We can classify most asynchronous js operations with 2 primary triggers.

//1. Browser API/WEB API events or functions. These include method like setTimeout, or event handlers like onclick, mouse hover, scroll and many more.


/*function printMe(){
	console.log("print me")
}
//example of asynchronous
setTimeout(printMe, 5000)

function print(){
	console.log("print meeeeeee")
}


function test(){
	console.log("test")
}


//mauuna si test dahil sa setTimeout
setTimeout(print, 4000)
test()

//another example
function f1(){
	console.log("f1")
}

function f2(){
	console.log("f2")
}

function main(){
	console.log("main")
	setTimeout(f1, 0)

	new Promise((resolve, reject) =>
		resolve("I am a promise")
		).then(resolve => console.log(resolve))

	f2()
}
//main, f2, I am promise, f1
*/
//main()
//output main, f2, f1
//CallBACK QUEUE (Data structure)
//in Javascript, we have a queue of callback functions. It is called a callback queue or task queue.
//A queue data structure is "First-in-First-out".

//Job Queue
//Every time a promise occurs in the code, the executer function gets into the job queue. The event loop works as usual, to look into the queues but gives priority to the job queue items over the callback queue items when the stack is free.

//mas mauuna si promise bago si setTimeout


//number2. Promises. A unique javascript object that allows us to perform.

/*new Promise((resolve, reject) => resolve("I am a promise")).then(resolve => console.log(resolve))*/

//REST API using a jsonplaceholder

//Getting all posts (GET method)

//Fetch => allows us to asynchronously request for a resource(data)
// we used the promise for async

/*
Syntax:

fetch("url", {optional objects})
.then(response => response.json()).then(data)


Optional object - which contains additional information about our requests such the method, the body, and the headers of our request
*/
//pag fulfilled kaya natin i access
console.log(fetch("https://jsonplaceholder.typicode.com/posts"))
//A promise may be in one of a three possible states: pending, fulfilled or rejected
//fetch ang URL
fetch("https://jsonplaceholder.typicode.com/posts").then(response => response.json())//parse the response as JSON
.then(data => {
	console.log(data)//process the result
}) 
/*{
	//method: "GET" or "POST"
}*/

//async await function
//wait can only use if there is async
async function fetchData(){
	//let's wait for the fetch method to complete then stores the value in a variable name
	let result = await fetch ("https://jsonplaceholder.typicode.com/posts")
	console.log(result)
	console.log(typeof result)
	//converts the result data from response
	let json = await result.json();
	console.log(json)
	console.log(typeof json)
}

fetchData()


//retrieving a specific posts


fetch("https://jsonplaceholder.typicode.com/posts/1").then(response => response.json())//parse the response as JSON
.then(data => {
	console.log(data)//process the result
})

//create a new post using POST Method




fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"	
	},
	//Sets the content/body of the "request" object to be sent to the backend.
//need to convert body to stringify
body: JSON.stringify({
	title: "New Post",
	body: "Hello Again",
	userId: 1
})
}).then(response => response.json())
.then(data => {
	console.log(data)
})


//Updating a POST
//PUT method - updates the whole document// nag ooverwrite talaga tayo.
//modifying resource where the client sends data that updates the entire resource.


//updating some properties only// partial updates only
//The patch method applies partial update to resource.
//lahat binabalik parin iuupdate lang if ano na update
//put all resources need to update
//patch if partial only that we want to update

// delete a post
//Method: "DELETE"
/*
Syntax:
//individual parameters
//"url?parameterName=value"
//Multilpe Parameters
	//url?parameterName=value&paramB=valueB

*/
//Filtering posts
fetch("https://jsonplaceholder.typicode.com/posts?userId=1").then(res => res.json()).then(data => console.log(data))

//Retrive a nested/related comments to posts
//Retrieving comments for a specific post(/post/:id/comments)

fetch('https://jsonplaceholder.typicode.com/posts/2/comments').then(res => res.json()).then(data => console.log(data))




async function fetchData(){

}





















































